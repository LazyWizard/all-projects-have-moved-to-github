# NOTICE #

All projects, including my Starsector mods, have been moved to GitHub. You can find their new location **[here](https://github.com/LazyWizard?tab=repositories)**.